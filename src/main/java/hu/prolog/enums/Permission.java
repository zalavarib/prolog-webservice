package hu.prolog.enums;

import java.util.Optional;
import java.util.stream.Stream;

public enum Permission {
	//@formatter:off
	ADD_PRODUCT("add_product"),
	DELETE_PRODUCT("delete_product"),
	LIST_PRODUCT("list_product"),
	CHANGE_USER_ROLE("change_user_role"),
	DELETE_USER("delete_user"),
	CHANGE_ROLE_PERMISSION("change_role_permission");
	//@formatter:on

	private String value;

	private Permission(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static Optional<Permission> getPermissionFromString(String permissionString) {
		return Stream.of(Permission.values()).filter(permission -> permissionString.equals(permission.getValue()))
				.findFirst();
	}

}
