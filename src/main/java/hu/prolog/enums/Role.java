package hu.prolog.enums;

import java.util.Optional;
import java.util.stream.Stream;

public enum Role {
	//@formatter:off
	ADMIN("admin_role"),
	USER("user_role");
	//@formatter:on

	private String value;

	private Role(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static Optional<Role> getRoleFromString(String roleString) {
		return Stream.of(Role.values()).filter(role -> roleString.equals(role.getValue())).findFirst();
	}

}
