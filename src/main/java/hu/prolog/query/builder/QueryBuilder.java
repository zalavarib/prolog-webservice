package hu.prolog.query.builder;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import hu.prolog.query.parameter.Parameter;

public abstract class QueryBuilder {

	public static String buildQuery(String methodName, Parameter... parameters) {
		if (parameters.length == 0) {
			return buildQuery(methodName);
		}

		return Stream.of(parameters).map(param -> param.getValue())
				.collect(Collectors.joining(", ", methodName + "(", ")."));
	}

	public static String buildQuery(String methodName) {
		return new StringBuilder(methodName).append(".").toString();
	}

	public static String concatQuerysWithAnd(String query1, String query2) {
		return concatQuery(query1, query2, ",");
	}

	public static String concatQuerysWithOr(String query1, String query2) {
		return concatQuery(query1, query2, ";");
	}

	public static String concatQuery(String query1, String query2, String delimiter) {
		return query1.replaceFirst("\\.$", delimiter + " " + query2);
	}

}
