package hu.prolog.query.parameter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class MultipleParameter implements Parameter {

	private final String prefix;
	private final String suffix;

	private List<Parameter> parameters;

	public MultipleParameter(List<Parameter> parameters, String prefix, String suffix) {
		this.prefix = prefix;
		this.suffix = suffix;
		this.parameters = parameters;
	}

	@Override
	public String getValue() {
		if (this.parameters == null || this.parameters.isEmpty()) {
			return new StringBuilder(this.prefix).append(this.suffix).toString();
		}

		return this.parameters.stream().map(param -> param.getValue())
				.collect(Collectors.joining(", ", this.prefix, this.suffix));
	}

	public boolean addParameter(Parameter parameter) {
		return this.parameters.add(parameter);
	}

	public boolean removeParameter(Parameter parameter) {
		return this.parameters.remove(parameter);
	}

	public List<Parameter> getParameters() {
		return Collections.unmodifiableList(this.parameters);
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

}
