package hu.prolog.query.parameter;

import java.util.List;

public class ParameterStruct extends MultipleParameter {

	public ParameterStruct(List<Parameter> parameters) {
		super(parameters, "(", ")");
	}

}
