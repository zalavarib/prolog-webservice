package hu.prolog.query.parameter;

public class SingleParameter implements Parameter {

	private String name;
	private boolean variable;

	public SingleParameter(String name) {
		this(name, false);
	}

	public SingleParameter(String name, boolean variable) {
		this.name = name;
		this.variable = variable;
	}

	@Override
	public String getValue() {
		if (this.variable) {
			return this.name;
		} else {
			return new StringBuilder("'").append(this.name).append("'").toString();
		}
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isVariable() {
		return this.variable;
	}

	public void setVariable(boolean variable) {
		this.variable = variable;
	}

}
