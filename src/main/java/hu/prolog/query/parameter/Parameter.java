package hu.prolog.query.parameter;

public interface Parameter {

	public String getValue();

}
