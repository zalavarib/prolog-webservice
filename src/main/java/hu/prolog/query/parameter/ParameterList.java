package hu.prolog.query.parameter;

import java.util.List;

public class ParameterList extends MultipleParameter {

	public ParameterList(List<Parameter> parameters) {
		super(parameters, "[", "]");
	}

}
