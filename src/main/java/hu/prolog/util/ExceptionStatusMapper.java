package hu.prolog.util;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import hu.prolog.service.exception.AlreadyHasTokenException;
import hu.prolog.service.exception.InvalidArgumentException;
import hu.prolog.service.exception.InvalidTokenException;
import hu.prolog.service.exception.LoginFailedException;
import hu.prolog.service.exception.PermissionDeniedException;
import hu.prolog.service.exception.ServiceException;
import hu.prolog.service.exception.UserAlreadyExistsException;

public class ExceptionStatusMapper {
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionStatusMapper.class);

	private static final HashMap<Class<? extends Exception>, HttpStatus> MAP;
	private static final HttpStatus DEFAULT_ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

	static {
		MAP = new HashMap<>();

		// status 4xx
		MAP.put(InvalidArgumentException.class, HttpStatus.BAD_REQUEST);
		MAP.put(AlreadyHasTokenException.class, HttpStatus.BAD_REQUEST);
		MAP.put(UserAlreadyExistsException.class, HttpStatus.BAD_REQUEST);
		MAP.put(LoginFailedException.class, HttpStatus.UNAUTHORIZED);
		MAP.put(InvalidTokenException.class, HttpStatus.UNAUTHORIZED);
		MAP.put(PermissionDeniedException.class, HttpStatus.FORBIDDEN);

		// status 5xxx
		MAP.put(ServiceException.class, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public static HttpStatus getStatus(Class<? extends Exception> clazz) {
		HttpStatus httpStatus = MAP.get(clazz);
		if (httpStatus == null) {
			LOG.warn("ExceptionStatusMapper does not contains this exception: " + clazz.getName());
			httpStatus = DEFAULT_ERROR_STATUS;
		}
		return httpStatus;
	}

}
