package hu.prolog.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import hu.prolog.bean.Product;

public class ArgumentValidator {

	public static final String[] INVALID_CHARACTERS = { "\'", "\\", "/" };

	public static boolean isValidArguments(String... arguments) {
		return isValidArguments(Arrays.asList(arguments));
	}

	public static boolean isInvalidArguments(String... arguments) {
		return isInvalidArguments(Arrays.asList(arguments));
	}

	public static boolean isValidArguments(List<String> arguments) {
		return !isInvalidArguments(arguments);
	}

	public static boolean isInvalidArguments(List<String> arguments) {
		return arguments.stream().anyMatch(ArgumentValidator::isInvalidArgument);
	}

	public static boolean isValidArgument(String argument) {
		return !isInvalidArgument(argument);
	}

	public static boolean isInvalidArgument(String argument) {
		return argument == null || Stream.of(INVALID_CHARACTERS).anyMatch(character -> argument.contains(character));
	}

	public static boolean isValidProducts(List<Product> products) {
		return !isInvalidProducts(products);
	}

	public static boolean isInvalidProducts(List<Product> products) {
		String[] productArgs = Stream.concat(products.stream().map(product -> product.getName()),
				products.stream().map(product -> product.getPlace())).toArray(String[]::new);

		return isInvalidArguments(productArgs);
	}

}
