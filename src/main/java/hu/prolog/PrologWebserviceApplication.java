package hu.prolog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrologWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrologWebserviceApplication.class, args);
	}
}
