package hu.prolog.global;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.prolog.service.web.AbstractRestController;

public class PrologConstant {
	private static final Logger LOG = LoggerFactory.getLogger(AbstractRestController.class);

	public static final String BASE_DIR;

	static {
		Properties props = new Properties();

		try (InputStream stream = new FileInputStream("./prolog.properties")) {
			props.load(stream);
		} catch (IOException e) {
			LOG.warn(e.getMessage(), e);
		}

		BASE_DIR = props.getProperty("prolog.base.dir");
	}

	public static final String CONSULT_USER_HANDLER_QUERY = "consult('" + BASE_DIR + "user/user_handler.pl').";

	public static final String CONSULT_TOKEN_HANDLER_QUERY = "consult('" + BASE_DIR + "token/token_handler.pl').";

	public static final String CONSULT_PRODUCT_HANDLER_QUERY = "consult('" + BASE_DIR + "product/product_handler.pl').";

	public static final String CONSULT_ROLE_PERMISSION_QUERY = "consult('" + BASE_DIR
			+ "authorization/authorization_handler.pl').";

	public static final String CHECK_USER_METHOD = "check_user";

	public static final String REGIST_METHOD = "add_user";

	public static final String ADD_TOKEN_METHOD = "add_token";

	public static final String DELETE_TOKEN_METHOD = "delete_token";

	public static final String CHECK_METHOD = "check";

	public static final String GET_PRODUCT_METHOD = "get_products";

	public static final String DELETE_PRODUCT_LIST_METHOD = "delete_product_list";

	public static final String ADD_PRODUCT_LIST_METHOD = "add_product_list";

	public static final String ADD_ROLE_TO_USER_METHOD = "add_role_to_user";

	public static final String REVOKE_ROLE_FROM_USER_METHOD = "revoke_role_from_user";

	public static final String ADD_PERMISSION_TO_ROLE_METHOD = "add_permission_to_role";

	public static final String REVOKE_PERMISSION_FROM_ROLE_METHOD = "revoke_permission_from_role";

	public static final String HAS_PERMISSION_METHOD = "user_has_permission";

	public static final String TOKEN_EXISTS_METHOD = "token_exists";

	public static final String DELETE_USER_METHOD = "delete_user_by_name";

	public static final String PRODUCT_NAME = "Product";

	public static final String PLACE_NAME = "Place";

	public static final String TOKEN = "Token";

}
