package hu.prolog.bean;

import java.io.Serializable;

public class Product implements Serializable {
	private static final long serialVersionUID = 6204701138927749614L;

	private String name;
	private String place;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}
