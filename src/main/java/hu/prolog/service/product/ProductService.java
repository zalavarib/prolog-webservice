package hu.prolog.service.product;

import java.util.List;

import hu.prolog.bean.Product;
import hu.prolog.service.exception.ServiceException;

public interface ProductService {

	public List<Product> getProductList(String token, String username) throws ServiceException;

	public List<Product> getProductListByName(String token, String username, String productName)
			throws ServiceException;

	public List<Product> getProductListByPlace(String token, String username, String place) throws ServiceException;

	public boolean isExists(String token, String username, Product product) throws ServiceException;

	public boolean deleteProductList(String token, String username, List<Product> products) throws ServiceException;

	public boolean addProductList(String token, String username, List<Product> products) throws ServiceException;
}
