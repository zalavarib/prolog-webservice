package hu.prolog.service.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.prolog.bean.Product;
import hu.prolog.enums.Permission;
import hu.prolog.service.AbstractPrologService;
import hu.prolog.service.authorization.AuthorizationService;
import hu.prolog.service.dao.ProductDao;
import hu.prolog.service.dao.exception.DaoException;
import hu.prolog.service.exception.ServiceException;
import hu.prolog.service.token.TokenService;

@Service
public class PrologProductService extends AbstractPrologService implements ProductService {

	@Autowired
	private ProductDao productDao;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private AuthorizationService authorizationService;

	@Override
	public List<Product> getProductList(String token, String username) throws ServiceException {
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.LIST_PRODUCT);

		try {
			return this.productDao.getProductList(username);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Product> getProductListByName(String token, String username, String productName)
			throws ServiceException {
		this.validateArguments(productName);
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.LIST_PRODUCT);

		try {
			return this.productDao.getProductListByName(username, productName);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Product> getProductListByPlace(String token, String username, String place) throws ServiceException {
		this.validateArguments(place);
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.LIST_PRODUCT);

		try {
			return this.productDao.getProductListByPlace(username, place);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean isExists(String token, String username, Product product) throws ServiceException {
		this.validateArguments(product.getName(), product.getPlace());
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.LIST_PRODUCT);

		try {
			return this.productDao.isExists(username, product);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean deleteProductList(String token, String username, List<Product> products) throws ServiceException {
		this.validateProducts(products);
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.DELETE_PRODUCT);

		try {
			return this.productDao.deleteProductList(username, products);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean addProductList(String token, String username, List<Product> products) throws ServiceException {
		this.validateProducts(products);
		this.tokenService.validateUserToken(token, username);
		this.authorizationService.validatePermission(username, Permission.ADD_PRODUCT);

		try {
			return this.productDao.addProductList(username, products);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
