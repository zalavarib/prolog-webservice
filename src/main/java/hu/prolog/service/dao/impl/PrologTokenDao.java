package hu.prolog.service.dao.impl;

import org.springframework.stereotype.Repository;

import hu.prolog.global.PrologConstant;
import hu.prolog.query.parameter.Parameter;
import hu.prolog.query.parameter.SingleParameter;
import hu.prolog.service.dao.AbstractPrologDao;
import hu.prolog.service.dao.TokenDao;
import hu.prolog.service.dao.exception.DaoException;

@Repository
public class PrologTokenDao extends AbstractPrologDao implements TokenDao {

	public PrologTokenDao() {
		super(PrologConstant.CONSULT_TOKEN_HANDLER_QUERY);
	}

	@Override
	public void addToken(String username, String token) throws DaoException {
		hasSolution(PrologConstant.ADD_TOKEN_METHOD, getTokenAndUsernameParameters(token, username));
	}

	@Override
	public boolean deleteToken(String token, String username) throws DaoException {
		return hasSolution(PrologConstant.DELETE_TOKEN_METHOD, getTokenAndUsernameParameters(token, username));
	}

	@Override
	public boolean hasValidToken(String username) throws DaoException {
		SingleParameter usernameParam = new SingleParameter(username);
		SingleParameter tokenParam = new SingleParameter(PrologConstant.TOKEN, true);

		return hasSolution(PrologConstant.CHECK_METHOD, tokenParam, usernameParam);
	}

	@Override
	public boolean checkUserToken(String token, String username) throws DaoException {
		return hasSolution(PrologConstant.CHECK_METHOD, getTokenAndUsernameParameters(token, username));
	}

	@Override
	public boolean isTokenExists(String token) throws DaoException {
		return hasSolution(PrologConstant.TOKEN_EXISTS_METHOD, new SingleParameter(token));
	}

	private static Parameter[] getTokenAndUsernameParameters(String token, String username) {
		return new Parameter[] { new SingleParameter(token), new SingleParameter(username) };
	}

}
