package hu.prolog.service.dao.impl;

import org.springframework.stereotype.Repository;

import hu.prolog.bean.User;
import hu.prolog.global.PrologConstant;
import hu.prolog.query.parameter.Parameter;
import hu.prolog.query.parameter.SingleParameter;
import hu.prolog.service.dao.AbstractPrologDao;
import hu.prolog.service.dao.UserDao;
import hu.prolog.service.dao.exception.DaoException;

@Repository
public class PrologUserDao extends AbstractPrologDao implements UserDao {

	public PrologUserDao() {
		super(PrologConstant.CONSULT_USER_HANDLER_QUERY);
	}

	@Override
	public boolean checkUser(User user) throws DaoException {
		return hasSolution(PrologConstant.CHECK_USER_METHOD, getUserParameters(user));
	}

	@Override
	public boolean addUser(User user) throws DaoException {
		return hasSolution(PrologConstant.REGIST_METHOD, getUserParameters(user));
	}

	@Override
	public boolean deleteUser(String user) throws DaoException {
		return hasSolution(PrologConstant.DELETE_USER_METHOD, new SingleParameter(user));
	}

	private static Parameter[] getUserParameters(User user) {
		return new Parameter[] { new SingleParameter(user.getUsername()), new SingleParameter(user.getPassword()) };
	}

}
