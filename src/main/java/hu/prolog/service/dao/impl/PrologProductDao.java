package hu.prolog.service.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jpl7.Term;
import org.springframework.stereotype.Repository;

import hu.prolog.bean.Product;
import hu.prolog.global.PrologConstant;
import hu.prolog.query.parameter.ParameterList;
import hu.prolog.query.parameter.ParameterStruct;
import hu.prolog.query.parameter.SingleParameter;
import hu.prolog.service.dao.AbstractPrologDao;
import hu.prolog.service.dao.ProductDao;
import hu.prolog.service.dao.exception.DaoException;

@Repository
public class PrologProductDao extends AbstractPrologDao implements ProductDao {

	public PrologProductDao() {
		super(PrologConstant.CONSULT_PRODUCT_HANDLER_QUERY);
	}

	@Override
	public List<Product> getProductList(String username) throws DaoException {
		SingleParameter usernameParameter = new SingleParameter(username);
		SingleParameter productParameter = new SingleParameter(PrologConstant.PRODUCT_NAME, true);
		SingleParameter placeParameter = new SingleParameter(PrologConstant.PLACE_NAME, true);

		Map<String, Term>[] allSolutions = allSolutions(PrologConstant.GET_PRODUCT_METHOD, usernameParameter,
				productParameter, placeParameter);

		return Stream.of(allSolutions).map(map -> {
			Product product = new Product();
			product.setName(map.get(PrologConstant.PRODUCT_NAME).name());
			product.setPlace(map.get(PrologConstant.PLACE_NAME).name());
			return product;
		}).collect(Collectors.toList());
	}

	@Override
	public List<Product> getProductListByName(String username, String productName) throws DaoException {
		SingleParameter usernameParameter = new SingleParameter(username);
		SingleParameter productParameter = new SingleParameter(productName);
		SingleParameter placeParameter = new SingleParameter(PrologConstant.PLACE_NAME, true);

		Map<String, Term>[] allSolutions = allSolutions(PrologConstant.GET_PRODUCT_METHOD, usernameParameter,
				productParameter, placeParameter);

		return Stream.of(allSolutions).map(map -> {
			Product product = new Product();
			product.setName(productName);
			product.setPlace(map.get(PrologConstant.PLACE_NAME).name());
			return product;
		}).collect(Collectors.toList());
	}

	@Override
	public List<Product> getProductListByPlace(String username, String place) throws DaoException {
		SingleParameter usernameParameter = new SingleParameter(username);
		SingleParameter productParameter = new SingleParameter(PrologConstant.PRODUCT_NAME, true);
		SingleParameter placeParameter = new SingleParameter(place);

		Map<String, Term>[] allSolutions = allSolutions(PrologConstant.GET_PRODUCT_METHOD, usernameParameter,
				productParameter, placeParameter);

		return Stream.of(allSolutions).map(map -> {
			Product product = new Product();
			product.setName(map.get(PrologConstant.PRODUCT_NAME).name());
			product.setPlace(place);
			return product;
		}).collect(Collectors.toList());
	}

	@Override
	public boolean isExists(String username, Product product) throws DaoException {
		ArrayList<Product> products = new ArrayList<>(1);
		products.add(product);

		SingleParameter usernameParameter = new SingleParameter(username);
		SingleParameter productParameter = new SingleParameter(product.getName());
		SingleParameter placeParameter = new SingleParameter(product.getPlace());

		return hasSolution(PrologConstant.GET_PRODUCT_METHOD, usernameParameter, productParameter, placeParameter);
	}

	@Override
	public boolean deleteProductList(String username, List<Product> products) throws DaoException {
		SingleParameter usernameParameter = new SingleParameter(username);
		ParameterList productList = this.getProductParameterList(products);

		return hasSolution(PrologConstant.DELETE_PRODUCT_LIST_METHOD, usernameParameter, productList);
	}

	@Override
	public boolean addProductList(String username, List<Product> products) throws DaoException {
		SingleParameter usernameParameter = new SingleParameter(username);
		ParameterList productList = this.getProductParameterList(products);

		return hasSolution(PrologConstant.ADD_PRODUCT_LIST_METHOD, usernameParameter, productList);
	}

	private ParameterList getProductParameterList(List<Product> products) {
		return new ParameterList(products.stream()
				.map(product -> new ParameterStruct(
						Arrays.asList(new SingleParameter(product.getName()), new SingleParameter(product.getPlace()))))
				.collect(Collectors.toList()));
	}

}
