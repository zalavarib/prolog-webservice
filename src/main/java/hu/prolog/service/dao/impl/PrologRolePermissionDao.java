package hu.prolog.service.dao.impl;

import org.springframework.stereotype.Repository;

import hu.prolog.enums.Permission;
import hu.prolog.enums.Role;
import hu.prolog.global.PrologConstant;
import hu.prolog.query.parameter.SingleParameter;
import hu.prolog.service.dao.AbstractPrologDao;
import hu.prolog.service.dao.RolePermissionDao;
import hu.prolog.service.dao.exception.DaoException;

@Repository
public class PrologRolePermissionDao extends AbstractPrologDao implements RolePermissionDao {

	public PrologRolePermissionDao() {
		super(PrologConstant.CONSULT_ROLE_PERMISSION_QUERY);
	}

	@Override
	public boolean grantRoleToUser(String user, Role role) throws DaoException {
		SingleParameter userParam = new SingleParameter(user);
		SingleParameter roleParam = new SingleParameter(role.getValue());

		return hasSolution(PrologConstant.ADD_ROLE_TO_USER_METHOD, userParam, roleParam);
	}

	@Override
	public boolean revokeRoleFromUser(String user, Role role) throws DaoException {
		SingleParameter userParam = new SingleParameter(user);
		SingleParameter roleParam = new SingleParameter(role.getValue());

		return hasSolution(PrologConstant.REVOKE_ROLE_FROM_USER_METHOD, userParam, roleParam);
	}

	@Override
	public boolean grantPermissionToRole(Role role, Permission permission) throws DaoException {
		SingleParameter roleParam = new SingleParameter(role.getValue());
		SingleParameter permParam = new SingleParameter(permission.getValue());

		return hasSolution(PrologConstant.ADD_PERMISSION_TO_ROLE_METHOD, permParam, roleParam);
	}

	@Override
	public boolean revokePermissionFromRole(Role role, Permission permission) throws DaoException {
		SingleParameter roleParam = new SingleParameter(role.getValue());
		SingleParameter permParam = new SingleParameter(permission.getValue());

		return hasSolution(PrologConstant.REVOKE_PERMISSION_FROM_ROLE_METHOD, permParam, roleParam);
	}

	@Override
	public boolean hasPermission(String user, Permission permission) throws DaoException {
		SingleParameter userParam = new SingleParameter(user);
		SingleParameter permParam = new SingleParameter(permission.getValue());

		return hasSolution(PrologConstant.HAS_PERMISSION_METHOD, userParam, permParam);
	}

}
