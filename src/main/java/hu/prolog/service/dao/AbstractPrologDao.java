package hu.prolog.service.dao;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.jpl7.JPLException;
import org.jpl7.Query;
import org.jpl7.Term;
import org.springframework.stereotype.Repository;

import hu.prolog.query.builder.QueryBuilder;
import hu.prolog.query.parameter.Parameter;
import hu.prolog.service.dao.exception.DaoException;

@Repository
public abstract class AbstractPrologDao {
	private final String consultMethod;

	public AbstractPrologDao(String consultMethod) {
		this.consultMethod = consultMethod;
	}

	@PostConstruct
	public void init() {
		Query.hasSolution(this.consultMethod);
	}

	protected static boolean hasSolution(String query) throws DaoException {
		try {
			return Query.hasSolution(query);
		} catch (JPLException e) {
			throw new DaoException(e);
		}
	}

	protected static boolean hasSolution(String method, Parameter... parameters) throws DaoException {
		try {
			return Query.hasSolution(QueryBuilder.buildQuery(method, parameters));
		} catch (JPLException e) {
			throw new DaoException(e);
		}
	}

	protected static Map<String, Term>[] allSolutions(String query) throws DaoException {
		try {
			return Query.allSolutions(query);
		} catch (JPLException e) {
			throw new DaoException(e);
		}
	}

	protected static Map<String, Term>[] allSolutions(String method, Parameter... parameters) throws DaoException {
		try {
			return Query.allSolutions(QueryBuilder.buildQuery(method, parameters));
		} catch (JPLException e) {
			throw new DaoException(e);
		}
	}

}
