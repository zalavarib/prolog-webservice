package hu.prolog.service.dao;

import hu.prolog.bean.User;
import hu.prolog.service.dao.exception.DaoException;

public interface UserDao {

	public boolean checkUser(User user) throws DaoException;

	public boolean addUser(User user) throws DaoException;

	public boolean deleteUser(String user) throws DaoException;

}
