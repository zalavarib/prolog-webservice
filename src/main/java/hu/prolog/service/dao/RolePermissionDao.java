package hu.prolog.service.dao;

import hu.prolog.enums.Permission;
import hu.prolog.enums.Role;
import hu.prolog.service.dao.exception.DaoException;

public interface RolePermissionDao {

	public boolean grantRoleToUser(String userToChange, Role role) throws DaoException;

	public boolean revokeRoleFromUser(String userToChange, Role role) throws DaoException;

	public boolean grantPermissionToRole(Role role, Permission permission) throws DaoException;

	public boolean revokePermissionFromRole(Role role, Permission permission) throws DaoException;

	public boolean hasPermission(String user, Permission permission) throws DaoException;

}
