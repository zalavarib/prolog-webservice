package hu.prolog.service.dao;

import hu.prolog.service.dao.exception.DaoException;

public interface TokenDao {

	public void addToken(String username, String token) throws DaoException;

	public boolean deleteToken(String token, String username) throws DaoException;

	public boolean hasValidToken(String username) throws DaoException;

	public boolean checkUserToken(String token, String username) throws DaoException;

	public boolean isTokenExists(String token) throws DaoException;

}
