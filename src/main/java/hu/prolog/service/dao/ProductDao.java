package hu.prolog.service.dao;

import java.util.List;

import hu.prolog.bean.Product;
import hu.prolog.service.dao.exception.DaoException;

public interface ProductDao {

	public List<Product> getProductList(String username) throws DaoException;

	public List<Product> getProductListByName(String username, String productName) throws DaoException;

	public List<Product> getProductListByPlace(String username, String place) throws DaoException;

	public boolean isExists(String username, Product product) throws DaoException;

	public boolean deleteProductList(String username, List<Product> products) throws DaoException;

	public boolean addProductList(String username, List<Product> products) throws DaoException;

}
