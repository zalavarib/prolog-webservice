package hu.prolog.service.authorization;

import hu.prolog.enums.Permission;
import hu.prolog.service.exception.ServiceException;

public interface AuthorizationService {

	public boolean hasPermission(String user, Permission permission) throws ServiceException;

	public void validatePermission(String user, Permission permission) throws ServiceException;

}
