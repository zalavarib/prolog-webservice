package hu.prolog.service.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.prolog.enums.Permission;
import hu.prolog.service.AbstractPrologService;
import hu.prolog.service.dao.RolePermissionDao;
import hu.prolog.service.dao.exception.DaoException;
import hu.prolog.service.exception.PermissionDeniedException;
import hu.prolog.service.exception.ServiceException;

@Service
public class PrologAuthorizationService extends AbstractPrologService implements AuthorizationService {

	@Autowired
	private RolePermissionDao rolePermissionDao;

	@Override
	public boolean hasPermission(String user, Permission permission) throws ServiceException {
		this.validateArguments(user);

		try {
			return this.rolePermissionDao.hasPermission(user, permission);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void validatePermission(String user, Permission permission) throws ServiceException {
		if (!this.hasPermission(user, permission)) {
			throw new PermissionDeniedException();
		}
	}

}
