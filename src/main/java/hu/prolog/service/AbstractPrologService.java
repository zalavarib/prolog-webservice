package hu.prolog.service;

import java.util.List;

import hu.prolog.bean.Product;
import hu.prolog.service.exception.InvalidArgumentException;
import hu.prolog.util.ArgumentValidator;

public abstract class AbstractPrologService {

	public void validateArguments(String... args) throws InvalidArgumentException {
		if (ArgumentValidator.isInvalidArguments(args)) {
			throw new InvalidArgumentException(InvalidArgumentException.DEFAULT_ERROR_MESSAGE);
		}
	}

	public void validateProducts(List<Product> products) throws InvalidArgumentException {
		if (ArgumentValidator.isInvalidProducts(products)) {
			throw new InvalidArgumentException(InvalidArgumentException.DEFAULT_ERROR_MESSAGE);
		}
	}

}
