package hu.prolog.service.exception;

public class InvalidArgumentException extends ServiceException {
	private static final long serialVersionUID = 3170137908851514348L;

	public static final String DEFAULT_ERROR_MESSAGE = "Argument contains invalid character or is not set.";

	public InvalidArgumentException() {
		super();
	}

	public InvalidArgumentException(String message) {
		super(message);
	}

	public InvalidArgumentException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidArgumentException(Throwable cause) {
		super(cause);
	}

	protected InvalidArgumentException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
