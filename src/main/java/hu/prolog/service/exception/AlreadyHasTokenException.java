package hu.prolog.service.exception;

public class AlreadyHasTokenException extends ServiceException {
	private static final long serialVersionUID = -8692917452225972453L;

	public AlreadyHasTokenException() {
		super();
	}

	public AlreadyHasTokenException(String message) {
		super(message);
	}

	public AlreadyHasTokenException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlreadyHasTokenException(Throwable cause) {
		super(cause);
	}

	protected AlreadyHasTokenException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
