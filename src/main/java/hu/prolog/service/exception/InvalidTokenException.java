package hu.prolog.service.exception;

public class InvalidTokenException extends ServiceException {
	private static final long serialVersionUID = 6586493393351548254L;

	public static final String DEFAULT_MESSAGE = "Token is invalid";

	public InvalidTokenException() {
		super();
	}

	public InvalidTokenException(String message) {
		super(message);
	}

	public InvalidTokenException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidTokenException(Throwable cause) {
		super(cause);
	}

	protected InvalidTokenException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
