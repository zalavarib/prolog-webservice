package hu.prolog.service.administration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.prolog.enums.Permission;
import hu.prolog.enums.Role;
import hu.prolog.service.AbstractPrologService;
import hu.prolog.service.authorization.AuthorizationService;
import hu.prolog.service.dao.RolePermissionDao;
import hu.prolog.service.dao.exception.DaoException;
import hu.prolog.service.exception.ServiceException;
import hu.prolog.service.token.TokenService;

@Service
public class PrologAdminService extends AbstractPrologService implements AdminService {

	@Autowired
	private AuthorizationService authorizationService;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private RolePermissionDao rolePermissionDao;

	@Override
	public Boolean grantRoleToUser(String user, String token, String userToChange, Role role) throws ServiceException {
		this.validateArguments(userToChange);
		this.tokenService.validateUserToken(token, user);
		this.authorizationService.validatePermission(user, Permission.CHANGE_USER_ROLE);

		try {
			return this.rolePermissionDao.grantRoleToUser(userToChange, role);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Boolean revokeRoleFromUser(String user, String token, String userToChange, Role role)
			throws ServiceException {
		this.validateArguments(userToChange);
		this.tokenService.validateUserToken(token, user);
		this.authorizationService.validatePermission(user, Permission.CHANGE_USER_ROLE);

		try {
			return this.rolePermissionDao.revokeRoleFromUser(userToChange, role);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Boolean grantPermissionToRole(String user, String token, Role role, Permission permission)
			throws ServiceException {
		this.tokenService.validateUserToken(token, user);
		this.authorizationService.validatePermission(user, Permission.CHANGE_ROLE_PERMISSION);

		try {
			return this.rolePermissionDao.grantPermissionToRole(role, permission);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Boolean revokePermissionFromRole(String user, String token, Role role, Permission permission)
			throws ServiceException {
		this.tokenService.validateUserToken(token, user);
		this.authorizationService.validatePermission(user, Permission.CHANGE_ROLE_PERMISSION);

		try {
			return this.rolePermissionDao.revokePermissionFromRole(role, permission);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
