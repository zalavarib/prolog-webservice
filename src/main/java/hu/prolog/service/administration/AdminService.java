package hu.prolog.service.administration;

import hu.prolog.enums.Permission;
import hu.prolog.enums.Role;
import hu.prolog.service.exception.ServiceException;

public interface AdminService {

	public Boolean grantRoleToUser(String user, String token, String userToChange, Role role) throws ServiceException;

	public Boolean revokeRoleFromUser(String user, String token, String userToChange, Role role)
			throws ServiceException;

	public Boolean grantPermissionToRole(String user, String token, Role role, Permission permission)
			throws ServiceException;

	public Boolean revokePermissionFromRole(String user, String token, Role role, Permission permission)
			throws ServiceException;

}
