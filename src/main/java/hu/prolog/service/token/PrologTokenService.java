package hu.prolog.service.token;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.prolog.service.AbstractPrologService;
import hu.prolog.service.dao.TokenDao;
import hu.prolog.service.dao.exception.DaoException;
import hu.prolog.service.exception.AlreadyHasTokenException;
import hu.prolog.service.exception.InvalidTokenException;
import hu.prolog.service.exception.ServiceException;

@Service
public class PrologTokenService extends AbstractPrologService implements TokenService {

	@Autowired
	private TokenDao tokenDao;

	@Override
	public String getToken(String username) throws ServiceException {
		this.validateArguments(username);

		String token = this.getToken();
		try {
			this.tokenDao.addToken(username, token);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		return token;
	}

	@Override
	public boolean deleteToken(String token, String username) throws ServiceException {
		this.validateArguments(token, username);

		try {
			return this.tokenDao.deleteToken(token, username);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean hasValidToken(String username) throws ServiceException {
		this.validateArguments(username);

		try {
			return this.tokenDao.hasValidToken(username);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void validateToGetToken(String username) throws ServiceException {
		if (this.hasValidToken(username)) {
			throw new AlreadyHasTokenException();
		}
	}

	@Override
	public void validateUserToken(String token, String username) throws ServiceException {
		this.validateArguments(token, username);

		try {
			if (!this.tokenDao.checkUserToken(token, username)) {
				throw new InvalidTokenException();
			}
		} catch (DaoException e) {
			throw new InvalidTokenException(e);
		}
	}

	@Override
	public boolean isTokenExits(String token) throws ServiceException {
		this.validateArguments(token);

		try {
			return this.tokenDao.isTokenExists(token);
		} catch (DaoException e) {
			throw new InvalidTokenException(e);
		}
	}

	@Override
	public String getToken() throws ServiceException {
		try {
			String token;
			do {
				token = UUID.randomUUID().toString();
			} while (this.tokenDao.isTokenExists(token));
			return token;
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
}
