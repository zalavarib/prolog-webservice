package hu.prolog.service.token;

import hu.prolog.service.exception.ServiceException;

public interface TokenService {

	public String getToken(String username) throws ServiceException;

	public boolean deleteToken(String token, String username) throws ServiceException;

	public boolean hasValidToken(String username) throws ServiceException;

	public void validateToGetToken(String username) throws ServiceException;

	public void validateUserToken(String token, String username) throws ServiceException;

	public boolean isTokenExits(String token) throws ServiceException;

	public String getToken() throws ServiceException;

}
