package hu.prolog.service.authentication;

import hu.prolog.bean.User;
import hu.prolog.service.exception.ServiceException;

public interface AuthenticationService {

	public Boolean deleteUser(String user, String token, String userToDelete) throws ServiceException;

	public String login(User user) throws ServiceException;

	public void validateUser(User user) throws ServiceException;

	public boolean logout(String token, String username) throws ServiceException;

	public void regist(User user) throws ServiceException;

}
