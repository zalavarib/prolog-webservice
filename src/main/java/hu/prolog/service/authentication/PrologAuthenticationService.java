package hu.prolog.service.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.prolog.bean.User;
import hu.prolog.enums.Permission;
import hu.prolog.service.AbstractPrologService;
import hu.prolog.service.authorization.AuthorizationService;
import hu.prolog.service.dao.UserDao;
import hu.prolog.service.dao.exception.DaoException;
import hu.prolog.service.exception.LoginFailedException;
import hu.prolog.service.exception.ServiceException;
import hu.prolog.service.exception.UserAlreadyExistsException;
import hu.prolog.service.token.TokenService;

@Service
public class PrologAuthenticationService extends AbstractPrologService implements AuthenticationService {

	@Autowired
	private TokenService tokenService;
	@Autowired
	private AuthorizationService authorizationService;
	@Autowired
	private UserDao userDao;

	@Override
	public Boolean deleteUser(String user, String token, String userToDelete) throws ServiceException {
		this.validateArguments(userToDelete);
		this.tokenService.validateUserToken(token, user);
		this.authorizationService.validatePermission(user, Permission.DELETE_USER);

		try {
			return this.userDao.deleteUser(userToDelete);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String login(User user) throws ServiceException {
		this.validateArguments(user.getUsername(), user.getPassword());
		this.tokenService.validateToGetToken(user.getUsername());
		this.validateUser(user);

		return this.tokenService.getToken(user.getUsername());

	}

	@Override
	public void validateUser(User user) throws ServiceException {
		this.validateArguments(user.getUsername(), user.getPassword());

		try {
			if (!this.userDao.checkUser(user)) {
				throw new LoginFailedException("Username or password is invalid: " + user.getUsername());
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean logout(String token, String username) throws ServiceException {
		return this.tokenService.deleteToken(token, username);
	}

	@Override
	public void regist(User user) throws ServiceException {
		this.validateArguments(user.getUsername(), user.getPassword());

		try {
			if (!this.userDao.addUser(user)) {
				throw new UserAlreadyExistsException(
						"The " + user.getUsername() + " username is in use, please choose an other one.");
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
