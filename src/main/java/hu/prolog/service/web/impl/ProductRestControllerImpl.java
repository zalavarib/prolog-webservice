package hu.prolog.service.web.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.prolog.bean.Product;
import hu.prolog.service.product.ProductService;
import hu.prolog.service.web.ProductRestController;
import hu.prolog.service.web.PrologRestController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ProductRestControllerImpl extends PrologRestController implements ProductRestController {

	@Autowired
	private ProductService productService;

	@Override
	@RequestMapping(path = "/product", method = RequestMethod.GET)
	@ApiOperation(value = "Get all products", notes = "You can list all products which is yours.", response = List.class, responseContainer = "list")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Here are your products.", response = List.class) })
	public ResponseEntity<List<Product>> getProducts(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username) {
		return this.buildResponse(respGen -> respGen.setBody(this.productService.getProductList(token, username)));
	}

	@Override
	@RequestMapping(path = "/product/name/{productName}/", method = RequestMethod.GET)
	@ApiOperation(value = "Get products", notes = "You can list all products which is yours by name.", response = List.class, responseContainer = "list")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Here are your products.", response = List.class) })
	public ResponseEntity<List<Product>> getProductsByName(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username,
			@PathVariable(name = "productName", required = true) String productName) {
		return this.buildResponse(
				respGen -> respGen.setBody(this.productService.getProductListByName(token, username, productName)));
	}

	@Override
	@RequestMapping(path = "/product/place/{place}/", method = RequestMethod.GET)
	@ApiOperation(value = "Get all products", notes = "You can list all products which is yours by place.", response = List.class, responseContainer = "list")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Here are your products.", response = List.class) })
	public ResponseEntity<List<Product>> getProductsByPlace(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username,
			@PathVariable(name = "place", required = true) String place) {
		return this.buildResponse(
				respGen -> respGen.setBody(this.productService.getProductListByPlace(token, username, place)));
	}

	@Override
	@RequestMapping(path = "/product", method = RequestMethod.POST)
	@ApiOperation(value = "Check product", notes = "You can check the product existence.", response = Boolean.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Here is your answer.", response = Boolean.class) })
	public ResponseEntity<Boolean> isProductExists(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username, @RequestBody(required = true) Product product) {
		return this.buildResponse(respGen -> respGen.setBody(this.productService.isExists(token, username, product)));
	}

	@Override
	@RequestMapping(path = "/product", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete a product", notes = "You can delete a product.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Deleted.") })
	public ResponseEntity<Void> deleteProducts(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username, @RequestBody(required = true) List<Product> products) {
		return this.buildResponse(respGen -> this.productService.deleteProductList(token, username, products));
	}

	@Override
	@RequestMapping(path = "/product", method = RequestMethod.PUT)
	@ApiOperation(value = "Save a product", notes = "You can save a product.")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Saved.") })
	public ResponseEntity<Void> addProducts(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username, @RequestBody(required = true) List<Product> products) {
		return this.buildResponse(HttpStatus.CREATED,
				respGen -> this.productService.addProductList(token, username, products));
	}

}
