package hu.prolog.service.web.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.prolog.enums.Permission;
import hu.prolog.enums.Role;
import hu.prolog.service.administration.AdminService;
import hu.prolog.service.authentication.AuthenticationService;
import hu.prolog.service.web.AdminRestController;
import hu.prolog.service.web.PrologRestController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AdminRestControllerImpl extends PrologRestController implements AdminRestController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private AuthenticationService authenticationService;

	// @formatter:off
	@Override
	@RequestMapping(value = "/deleteUser/{userToDelete}/", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete user", notes = "You can delete a user.")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Deleted.") })
	public ResponseEntity<Void> deleteUser(@RequestHeader(required = true) String user,
										   @RequestHeader(required = true) String token,
										   @PathVariable(name = "userToDelete", required = true) String userToDelete) {
		return this.buildResponse(responseGenerator -> this.authenticationService.deleteUser(user, token, userToDelete));
	}

	@Override
	@RequestMapping(value = "/grantRole/{userToChange}/{role}/", method = RequestMethod.PUT)
	@ApiOperation(value = "Grant role", notes = "You can grant a role to a user.")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Granted.") })
	public ResponseEntity<Void> addRoleToUser(@RequestHeader(required = true) String user,
											  @RequestHeader(required = true) String token,
											  @PathVariable(name = "userToChange", required = true) String userToChange,
											  @PathVariable(name = "role", required = true) String role) {
		return this.buildResponse(HttpStatus.CREATED, responseGenerator -> {
			Optional<Role> roleFromString = Role.getRoleFromString(role);
			if (roleFromString.isPresent()) {
				this.adminService.grantRoleToUser(user, token, userToChange, roleFromString.get());
			} else {
				responseGenerator.setStatus(HttpStatus.BAD_REQUEST);
			}
		});
	}

	@Override
	@RequestMapping(value = "/revokeRole/{userToChange}/{role}/", method = RequestMethod.DELETE)
	@ApiOperation(value = "Revoke role", notes = "You can revoke a role from a user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Revoked.") })
	public ResponseEntity<Void> revokeRoleFromUser(@RequestHeader(required = true) String user,
												   @RequestHeader(required = true) String token,
												   @PathVariable(name = "userToChange", required = true) String userToChange,
												   @PathVariable(name = "role", required = true) String role) {
		return this.buildResponse(responseGenerator -> {
			Optional<Role> roleFromString = Role.getRoleFromString(role);
			if (roleFromString.isPresent()) {
				this.adminService.revokeRoleFromUser(user, token, userToChange, roleFromString.get());
			} else {
				responseGenerator.setStatus(HttpStatus.BAD_REQUEST);
			}
		});
	}

	@Override
	@RequestMapping(value = "/grantPermission/{role}/{permission}/", method = RequestMethod.PUT)
	@ApiOperation(value = "Grant permission", notes = "You can grant a permission to a role.")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Granted.") })
	public ResponseEntity<Void> addPermissionToRole(@RequestHeader(required = true) String user,
													@RequestHeader(required = true) String token,
													@PathVariable(name = "role", required = true) String role,
													@PathVariable(name = "permission", required = true) String permission) {
		return this.buildResponse(HttpStatus.CREATED, responseGenerator -> {
			Optional<Role> roleFromString = Role.getRoleFromString(role);
			Optional<Permission> permissionFromString = Permission.getPermissionFromString(permission);
			if (roleFromString.isPresent() && permissionFromString.isPresent()) {
				this.adminService.grantPermissionToRole(user, token, roleFromString.get(), permissionFromString.get());
			} else {
				responseGenerator.setStatus(HttpStatus.BAD_REQUEST);
			}
		});
	}

	@Override
	@RequestMapping(value = "/revokePermission/{role}/{permission}/", method = RequestMethod.DELETE)
	@ApiOperation(value = "Revoke permission", notes = "You can revoke a permission from a role.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Revoked.") })
	public ResponseEntity<Void> revokePermissionFromRole(@RequestHeader(required = true) String user,
														 @RequestHeader(required = true) String token,
														 @PathVariable(name = "role", required = true) String role,
														 @PathVariable(name = "permission", required = true) String permission) {
		return this.buildResponse(responseGenerator -> {
			Optional<Role> roleFromString = Role.getRoleFromString(role);
			Optional<Permission> permissionFromString = Permission.getPermissionFromString(permission);
			if (roleFromString.isPresent() && permissionFromString.isPresent()) {
				this.adminService.revokePermissionFromRole(user, token, roleFromString.get(),
						permissionFromString.get());
			} else {
				responseGenerator.setStatus(HttpStatus.BAD_REQUEST);
			}
		});
	}
	// @formatter:on
}
