package hu.prolog.service.web.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.prolog.bean.User;
import hu.prolog.service.authentication.AuthenticationService;
import hu.prolog.service.web.AuthRestController;
import hu.prolog.service.web.PrologRestController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AuthenticationRestControllerImpl extends PrologRestController implements AuthRestController {

	@Autowired
	private AuthenticationService authService;

	@Override
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	@ApiOperation(value = "Login", notes = "You can log in, and get a token.", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Here is your token.", response = String.class) })
	public ResponseEntity<String> login(@RequestBody(required = true) User user) {
		return this.buildResponse(responseGenerator -> responseGenerator.setBody(this.authService.login(user)));
	}

	@Override
	@RequestMapping(path = "/logout", method = RequestMethod.POST)
	@ApiOperation(value = "Logout", notes = "You can log out.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Your token is deleted.") })
	public ResponseEntity<Void> logout(@RequestHeader(required = true) String token,
			@RequestHeader(required = true) String username) {
		return this.buildResponse(responseGenerator -> this.authService.logout(token, username));
	}

	@Override
	@RequestMapping(path = "/regist", method = RequestMethod.PUT)
	@ApiOperation(value = "Regist", notes = "You can regist.")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Your are registed, now you can log in.") })
	public ResponseEntity<Void> regist(@RequestBody(required = true) User user) {
		return this.buildResponse(HttpStatus.CREATED, responseGenerator -> this.authService.regist(user));
	}

}
