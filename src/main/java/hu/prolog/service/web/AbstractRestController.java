package hu.prolog.service.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import hu.prolog.service.exception.ServiceException;
import hu.prolog.service.web.response.ResponseBuilder;
import hu.prolog.service.web.response.ResponseGenerator;
import hu.prolog.util.ExceptionStatusMapper;

@RestController
public abstract class AbstractRestController {
	private static final Logger LOG = LoggerFactory.getLogger(AbstractRestController.class);

	protected static final String HEADER_DESCRIPTION_KEY = "Description";

	public <T> ResponseEntity<T> buildResponse(ResponseBuilder<T> builder) {
		return this.buildResponse(HttpStatus.OK, builder);
	}

	public <T> ResponseEntity<T> buildResponse(HttpStatus succesStatus, ResponseBuilder<T> builder) {
		T body = null;
		HttpHeaders headers = new HttpHeaders();
		ResponseGenerator<T> responseGenerator = new ResponseGenerator<>(body, headers, succesStatus);
		try {
			builder.build(responseGenerator);
		} catch (ServiceException e) {
			LOG.info(e.getMessage(), e);

			responseGenerator.setStatus(ExceptionStatusMapper.getStatus(e.getClass()));
			if (e.getMessage() != null) {
				responseGenerator.addHeader(HEADER_DESCRIPTION_KEY, e.getMessage());
			}
		}
		return responseGenerator.generate();
	}
}
