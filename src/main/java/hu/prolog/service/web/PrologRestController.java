package hu.prolog.service.web;

import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.Api;

@Api("/prolog")
@RequestMapping(value = "/prolog")
public abstract class PrologRestController extends AbstractRestController {
}
