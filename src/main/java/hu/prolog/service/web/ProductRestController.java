package hu.prolog.service.web;

import java.util.List;

import org.springframework.http.ResponseEntity;

import hu.prolog.bean.Product;

public interface ProductRestController {

	public ResponseEntity<List<Product>> getProducts(String token, String username);

	public ResponseEntity<List<Product>> getProductsByName(String token, String username, String productName);

	public ResponseEntity<List<Product>> getProductsByPlace(String token, String username, String place);

	public ResponseEntity<Boolean> isProductExists(String token, String username, Product product);

	public ResponseEntity<Void> deleteProducts(String token, String username, List<Product> products);

	public ResponseEntity<Void> addProducts(String token, String username, List<Product> products);

}
