package hu.prolog.service.web;

import org.springframework.http.ResponseEntity;

import hu.prolog.bean.User;

public interface AuthRestController {

	public ResponseEntity<String> login(User user);

	public ResponseEntity<Void> logout(String token, String username);

	public ResponseEntity<Void> regist(User user);

}
