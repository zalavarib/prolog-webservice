package hu.prolog.service.web.response;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseGenerator<T> {

	private T body;
	private HttpStatus status;
	private HttpHeaders headers;

	public ResponseGenerator(T body, HttpHeaders headers, HttpStatus status) {
		this.body = body;
		this.headers = headers;
		this.status = status;
	}

	public ResponseEntity<T> generate() {
		return new ResponseEntity<T>(this.body, this.headers, this.status);
	}

	public void addHeader(String headerName, String headerValue) {
		this.headers.add(headerName, headerValue);
	}

	public List<String> removeHeader(Object key) {
		return this.headers.remove(key);
	}

	public boolean removeHeader(Object key, Object value) {
		return this.headers.remove(key, value);
	}

	public T getBody() {
		return this.body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	public HttpStatus getStatus() {
		return this.status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public HttpHeaders getHeaders() {
		return this.headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}
}
