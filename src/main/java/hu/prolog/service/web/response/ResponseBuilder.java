package hu.prolog.service.web.response;

import hu.prolog.service.exception.ServiceException;

public interface ResponseBuilder<T> {

	public void build(ResponseGenerator<T> responseGenerator) throws ServiceException;

}