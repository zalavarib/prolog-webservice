package hu.prolog.service.web;

import org.springframework.http.ResponseEntity;

public interface AdminRestController {

	public ResponseEntity<Void> deleteUser(String user, String token, String userToDelete);

	public ResponseEntity<Void> addRoleToUser(String user, String token, String userToChange, String role);

	public ResponseEntity<Void> revokeRoleFromUser(String user, String token, String userToChange, String role);

	public ResponseEntity<Void> addPermissionToRole(String user, String token, String role, String permission);

	public ResponseEntity<Void> revokePermissionFromRole(String user, String token, String role, String permission);

}
