to launch on linux add -Djava.library.path="/usr/lib/swipl-7.4.2/lib/x86_64-linux" to VM arguments
add add LD_PRELOAD /usr/lib/swipl-7.4.2/lib/x86_64-linux/libswipl.so to environment variable

to build need to install jpl 7.4.0 to local repository:
mvn install:install-file -Dfile=/usr/lib64/swipl-7.5.7/lib/jpl.jar -DgroupId=jpl -DartifactId=jpl -Dversion=7.4.0 -Dpackaging=jar -DgeneratePom=true

application.properties and prolog.properties must be next to the jar
