:- consult('../util/data_writer.pl').
:- consult('../util/util.pl').
:- consult('products.pl').

product_file('product/products.pl').

get_products(User, Product, Place) :- product(User, Product, Place).

add_product_list(User, ProductList) :- is_set(ProductList), !,
                                       no_duplicate_products(User, ProductList), !,
                                       store_product_list(User, ProductList),
                                       write_products.

delete_product_list(_, []) :- write_products.
delete_product_list(User, [(Product, Place) | ProductList]) :- retractall(product(User, Product, Place)), delete_product_list(User, ProductList).

delete_all_product(User) :- retractall(product(User, _, _)), write_products.

store_product_list(_, []).
store_product_list(User, [Product | ProductList]) :- store_product(User, Product), store_product_list(User, ProductList).

store_product(User, (Product, Place)) :- asserta(product(User, Product, Place)).

no_duplicate_products(User, [(Product, Place) | Products]) :- \+ product(User, Product, Place), !, no_duplicate_products(User, Products).
no_duplicate_products(_, []).
no_duplicate_products(_, _) :- fail.

write_products :- product_file(File), write_data(File, product/3).
