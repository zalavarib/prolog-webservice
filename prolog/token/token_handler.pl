:- consult('../util/data_writer.pl').
:- consult('../constant/constant.pl').
:- consult('tokens.pl').

token_file('token/tokens.pl').
date_format('%Y-%m-%dT%H:%MZ').

token_exists(Token) :- token(Token, _, _).

add_token(Token, Username) :- retractall(token(_, Username, _)),
                              get_current_time(CurrentTimeString),
                              asserta(token(Token, Username, CurrentTimeString)),
                              write_tokens.

delete_token(Token, Username) :- token(Token, Username, _),
                                 retractall(token(Token, Username, _)),
                                 write_tokens.

check(Token, Username) :- token(Token, Username, OldTimeString),
                          get_current_time(CurrentTimeString),
                          check_date_time(OldTimeString, CurrentTimeString),
                          rewrite_token(Token, Username, OldTimeString, CurrentTimeString), !.

check_date_time(OldTimeString, CurrentTimeString) :- parse_time(OldTimeString, OldTime),parse_time(CurrentTimeString, CurrentTime),
                                                     time_out(TIME_OUT), CurrentTime - OldTime =< TIME_OUT.

rewrite_token(Token, Username, OldTimeString, CurrentTimeString) :- retract(token(Token, Username, OldTimeString)),
                                                                    asserta(token(Token, Username, CurrentTimeString)),
                                                                    write_tokens.

write_tokens :- token_file(File), write_data(File, token/3).

delete_user_token(Username) :- retractall(token(_, Username, _)), write_tokens.

get_current_time(CurrentTimeString) :- get_time(CurrentTime), date_format(Format), format_time(atom(CurrentTimeString), Format, CurrentTime).
