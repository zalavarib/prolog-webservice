:- consult('../constant/constant.pl').
:- consult('../util/data_writer.pl').
:- consult('../util/util.pl').
:- consult('permission.pl').
:- consult('role.pl').

role_file('authorization/role.pl').
permission_file('authorization/permission.pl').

user_has_permission(User, Permission) :- roles(List), user_has_permission(List, User, Permission).

user_has_permission([], _, _) :- fail.
user_has_permission([Role | B], User, Permission) :- user_has_role(User, Role), role_has_permission(Permission, Role), !
                                                     ;
                                                     user_has_permission(B, User, Permission).

add_permission_to_role(Permission, Role) :- \+ role_has_permission(Permission, Role),
                                            is_role_exists(Role), is_permission_exists(Permission),
                                            exec_data(Role, Permission, 'assertz'), write_permission.

revoke_permission_from_role(Permission, Role) :- role_has_permission(Permission, Role),
                                                 is_role_exists(Role), is_permission_exists(Permission),
                                                 exec_data(Role, Permission, 'retractall'), write_permission.

add_role_to_user(User, Role) :- \+ user_has_role(User, Role), is_role_exists(Role),
                                exec_data(User, Role, 'assertz'),
                                write_role.

revoke_role_from_user(User, Role) :- user_has_role(User, Role), is_role_exists(Role),
                                     exec_data(User, Role, 'retractall'),
                                     write_role.

revoke_all_roles_from_user(User) :- roles(List), revoke_roles_from_user(User, List), !.

revoke_roles_from_user(_, []).
revoke_roles_from_user(User, [Role | B]) :- user_has_role(User, Role), revoke_role_from_user(User, Role), revoke_roles_from_user(User,  B).
revoke_roles_from_user(User, [_ | B]) :- revoke_roles_from_user(User,  B).

role_has_permission(Permission, Role) :- exec(Permission, Role).
user_has_role(User, Role) :- exec(Role, User).

is_permission_exists(Perm) :- permissions(List), once(member(Perm, List)).
is_role_exists(Role) :- roles(List), once(member(Role, List)).

write_role :- roles(List), role_file(File), write_list(File, List).

write_permission :- permissions(List), permission_file(File), write_list(File, List).
