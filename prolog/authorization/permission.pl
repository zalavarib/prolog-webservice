:- dynamic add_product/1.

add_product(user_role).
add_product(admin_role).

:- dynamic delete_product/1.

delete_product(user_role).
delete_product(admin_role).

:- dynamic list_product/1.

list_product(user_role).
list_product(admin_role).

:- dynamic change_user_role/1.

change_user_role(admin_role).

:- dynamic delete_user/1.

delete_user(admin_role).

:- dynamic change_role_permission/1.

change_role_permission(admin_role).

