:- consult('../authorization/authorization_handler.pl').
:- consult('../token/token_handler.pl').
:- consult('../constant/constant.pl').
:- consult('../util/data_writer.pl').
:- consult('../util/security.pl').
:- consult('users.pl').

user_file('user/users.pl').

change_password(Username, Password) :- user(Username, _), hash(Password, HashedPassword),
                                       retractall(user(Username, _)), asserta(user(Username, HashedPassword)),
                                       write_users.

delete_user_by_name(Username) :- user(Username, _),
                                 delete_token(_, Username),
                                 revoke_all_roles_from_user(Username), write_role,
								 delete_all_product(Username),
                                 retractall(user(Username, _)), write_users.

add_user(Username, Password) :- \+ user(Username, _),
                                hash(Password, HashedPassword),
                                asserta(user(Username, HashedPassword)),
                                default_role(Role), add_role_to_user(Username, Role),
                                write_role, write_users.

write_users :- user_file(File), write_data(File, user/2).

check_user(Username, Password) :- hash(Password, HashedPassword),
                                  user(Username, HashedPassword).
