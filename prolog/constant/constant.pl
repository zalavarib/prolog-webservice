time_out(1000).
hash_alogrithm(algorithm(sha512)).
base_dir('/home/zalavarib/Documents/prolog/prolog-webservice/prolog/').

default_role('user_role').

roles([
       'user_role',
       'admin_role'
      ]).

permissions([
             'add_product',
             'delete_product',
             'list_product',
             'change_user_role',
             'delete_user',
			 'change_role_permission'
            ]).
