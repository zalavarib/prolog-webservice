:- consult('util.pl').
:- consult('../constant/constant.pl').

write_data(File, Method) :- create_file_path(File, FilePath), tell(FilePath), listing(Method), told.

write_list(File, List) :- create_file_path(File, FilePath), tell(FilePath), listing_more(List), told.

create_file_path(File, FilePath) :- base_dir(Dir), atom_concat(Dir, File, FilePath).