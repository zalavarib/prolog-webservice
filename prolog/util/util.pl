concat_list(List, Res) :- reverse(List, RList), concat_list(RList, '', Res).
concat_list([], Res, Res).
concat_list([H | B], Str, Res) :- string_concat(H, Str, NewStr), concat_list(B, NewStr, Res).

listing_more([]).
listing_more([Method | B]) :- listing(Method), listing_more(B).

exec_data(Param, Method, QName) :- concat_list([QName, '(', Method, '(\'', Param, '\'))'], Q), exec(Q).

exec(Name, Param) :- concat_list([Name, '(\'', Param, '\')'], Q), exec(Q).

exec(Query) :- term_to_atom(A, Query), catch(A, _, fail).

is_set(List) :- \+ (select(Element, List, NewList), memberchk(Element, NewList)).
